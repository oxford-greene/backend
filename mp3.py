from filebase import FileBase
import os

class MP3(FileBase):
    def __init__(self, filename):
        self.currchunk = 0
        self.chunksize = 100
        self.fd = None
        self.filename = filename
        self.filesize = 0
        super().__init__()

    def open(self):
        self.filesize = os.path.getsize(self.filename)
        self.fd = open(self.filename, "rb")
        return True

    def nextchunk(self):
        if not self.fd:
            return ""
        data = self.fd.read(self.chunksize)
        self.currchunk += 1
        return data

    def done(self):
        return self.filesize - self.currchunk * self.chunksize > 0

    def close(self):
        self.fd.close()

    def encode_length(self):
        return "{:05d}".format(len(self.chunk)).encode("ascii")

