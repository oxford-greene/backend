from abc import ABC, abstractmethod

class FileBase(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def open(self):
        pass

    @abstractmethod
    def nextchunk(self):
        pass

    @abstractmethod
    def done(self):
        pass

