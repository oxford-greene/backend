import socket
import ssl
import sys
import traceback

import pdb

from mp3 import MP3

HOST = "127.0.0.1"
PORT = 3000
FILENAME = "test.mp3"

class Errors:
    CONN_LOST = -1
    FAILED_SEND = -2

class Response:
    OK = 0
    SENT_FILE = 1

def ssl_request(packet):
    # SET VARIABLES
    reply = ""

    # CREATE SOCKET
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(10)

    # WRAP SOCKET
    wrappedSocket = ssl.wrap_socket(sock, ssl_version=ssl.PROTOCOL_TLSv1, ciphers="ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS")

    # CONNECT AND PRINT REPLY
    wrappedSocket.connect((HOST, PORT))
    wrappedSocket.send(packet)
    reply = wrappedSocket.recv(1280)

    # CLOSE SOCKET CONNECTION
    wrappedSocket.close()
    return reply

class Server:
    def __init__(self, host, port):
        self.port = port
        self.host = host
        self.sock = None

    def sendfile(self, sock=self.sock):
        print("going to send file")
        song = MP3(FILENAME)
        song.open()
        while not song.done():
            print("reading...")
            pdb.set_trace()
            chunk = song.nextchunk()
            print(song.encode_length())
            chunk = song.nextchunk()
            print(chunk)
            sock.send(song.encode_length())
            sock.send(chunk)
        song.close()


    def processreq(self):
        data = self.sock.recv(50)
        if not data:
            # fatal?
            print("Connection Closed by middle end")
            return Errors.CONN_LOST
        elif data == b"gimme":
            self.sendfile()
            return Response.SENT_FILE
        else:
            print(data)
            return Response.OK

    def connect_to_middle(self):
        print("Attempting Connection to Middle End. HOST: %s, PORT: %i" % (HOST, PORT))
        connectionrequest = "pls accept"
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        fatal = False
        while not fatal:
            try:
                self.sock.connect((HOST, PORT))
            except:
                print("Could not connect to the Middle End.")
                sys.exit(1)
            print("Connected to middle end.")
            self.sock.send(connectionrequest.encode('UTF-8'))
            try:
                while True:
                    retval = self.processreq()
                    if retval == Errors.CONN_LOST:
                        break

            except socket.timeout:
                print("Socket Timed out. Attempting Reconnect...")
            except KeyboardInterrupt:
                fatal = True
                print("Bye.")
            except:
                traceback.print_exc()
                fatal = True
                print("Back end errored. Shutting down.")
            finally:
                self.sock.shutdown(socket.SHUT_RDWR)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("-p", "--port", type=int, default=PORT)
    parser.add_argument("-o", "--host", default=HOST)
    args = parser.parse_args()
    Server(args.host, args.port).connect_to_middle()

